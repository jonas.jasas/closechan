# CloseChan

CloseChan implements thread safe closing of the open or closed channel.
Close method returns `true` if closing operation was made on the open channel.

[![go report card](https://goreportcard.com/badge/gitlab.com/jonas.jasas/closechan)](https://goreportcard.com/report/gitlab.com/jonas.jasas/closechan)
[![pipeline status](https://gitlab.com/jonas.jasas/closechan/badges/master/pipeline.svg)](https://gitlab.com/jonas.jasas/closechan/commits/master)
[![coverage report](https://gitlab.com/jonas.jasas/closechan/badges/master/coverage.svg)](https://gitlab.com/jonas.jasas/closechan/commits/master)
[![godoc](https://godoc.org/gitlab.com/jonas.jasas/closechan?status.svg)](http://godoc.org/gitlab.com/jonas.jasas/closechan)