package closechan

import (
	"testing"
)

func TestDoubleClose(t *testing.T) {
	c := NewCloseChan()

	if c.Close() {
		if c.Close() {
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func TestBufChan(t *testing.T) {
	c := NewCloseBufChan(1)

	select {
	case c.C() <- struct{}{}:
	default:
		t.FailNow()
	}

	select {
	case c.C() <- struct{}{}:
		t.FailNow()
	default:
	}

	if c.Close() {
		if c.Close() {
			t.FailNow()
		}
	} else {
		t.FailNow()
	}
}

func TestClosed(t *testing.T) {
	c := NewCloseChan()

	if c.Closed() {
		t.FailNow()
	} else {
		close(c.C())
		if !c.Closed() {
			t.FailNow()
		}
	}
}
