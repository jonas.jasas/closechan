package closechan

import "sync"

// CloseChan is a signaling channel with the thread safe close function.
type CloseChan struct {
	c chan struct{}
	l sync.Mutex
}

// NewCloseChan creates CloseChan instance.
func NewCloseChan() *CloseChan {
	return &CloseChan{
		c: make(chan struct{}),
	}
}

// NewCloseBufChan creates CloseChan instance with the provided channel buffer length.
func NewCloseBufChan(length int) *CloseChan {
	return &CloseChan{
		c: make(chan struct{}, length),
	}
}

// C returns channel
func (cc *CloseChan) C() chan struct{} {
	return cc.c
}

// Close is closing channel in thread safe manner.
// Returns true if channel is closed, false if it has been previously closed.
func (cc *CloseChan) Close() (ok bool) {
	cc.l.Lock()
	defer cc.l.Unlock()

	select {
	case _, ok = <-cc.c:
	default:
		ok = true
	}

	if ok {
		close(cc.c)
	}

	return
}

// Checks if channel is closed.
// Returns true if channel at the current moment is closed.
// ATTENTION: Checking don't guarantee that channel will not be closed even before function returns result.
func (cc *CloseChan) Closed() (closed bool) {
	select {
	case _, ok := <-cc.c:
		closed = !ok
	default:
	}

	return
}
